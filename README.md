# python-tests


## Description
A simple repo for testing python problems.


## How to start
1. Clone the project with "git clone https://gitlab.com/robin_kim/python-tests.git"
2. Activate virtual environment
3. Solve some problems
4. To test your answers, type into your console 'python -m pytest -k "001"', excluding the outer single ''. As you solve from problem 001 to 002 and on, make sure to change the command from 'python -m pytest -k "001"' to 'python -m pytest -k "002"' and so on.
5. Goodluck!
