from unittest import TestCase
from problems.problem_004 import ceiling


class ProblemTests(TestCase):
    def test_1(self):
        value = ceiling(7, 3)
        self.assertEqual(4, value)
