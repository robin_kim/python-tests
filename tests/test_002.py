from unittest import TestCase
from problems.problem_002 import absolute_value


class ProblemTests(TestCase):
    def test_returns_abs_value_1(self):
        value = absolute_value(9, 8, 7)
        self.assertEqual(9, value)

    def test_returns_abs_value_2(self):
        value = absolute_value(1, 7, 3)
        self.assertEqual(7, value)

    def test_returns_abs_value_3(self):
        value = absolute_value(1, 2, 3)
        self.assertEqual(3, value)

    def test_returns_abs_value_4(self):
        value = absolute_value(8, 8, 7)
        self.assertEqual(8, value)

    def test_returns_abs_value_5(self):
        value = absolute_value(4, 6, 6)
        self.assertEqual(6, value)

    def test_returns_abs_value_6(self):
        value = absolute_value(7, 2, 7)
        self.assertEqual(7, value)

    def test_returns_abs_value_7(self):
        value = absolute_value(3, 3, 3)
        self.assertEqual(3, value)
