from unittest import TestCase
from problems.problem_001 import mini

class ProblemTest(TestCase):
    def test_returns_min_value_1(self):
        value = mini(1, 3)
        self.assertEqual(1, value)

    def test_returns_min_value_2(self):
        value = min(3, 2)
        self.assertEqual(2, value)

    def test_returns_min_value_3(self):
        value = min(5, 3)
        self.assertEqual(3, value)
