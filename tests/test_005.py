from unittest import TestCase
from problems.problem_005 import floor


class ProblemTests(TestCase):
    def test_1(self):
        value = floor(8, 3)
        self.assertEqual(2, value)
