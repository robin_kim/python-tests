from unittest import TestCase
from problems.problem_006 import random_int


class ProblemTests(TestCase):
    def test_1(self):
        value = random_int()
        self.assertIsInstance(value, list)
        self.assertEqual(len(value), 3)
        self.assertEqual(len(set(value)), 3)
