from unittest import TestCase
from problems.problem_009 import duplicate


class ProblemTests(TestCase):
    def test_1(self):
        value = duplicate("aabc")
        self.assertEqual(value, "abc")

    def test_2(self):
        value = duplicate("ttsestainging")
        self.assertEqual(value, "tea")

    def test_3(self):
        value = duplicate("azzbycctxn")
        self.assertEqual(value, "abytxn")
