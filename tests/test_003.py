from unittest import TestCase
from problems.problem_003 import get_index


class ProblemTests(TestCase):
    def test_1(self):
        value = get_index(['a', 'e', 'i', 'o', 'i', 'u'])
        self.assertEqual(1, value)
