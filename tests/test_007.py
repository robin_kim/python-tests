from unittest import TestCase
from problems.problem_007 import make_pasta


class ProblemTests(TestCase):
    def test_1(self):
        value = make_pasta(["flour", "eggs", "oil"])
        self.assertEqual(value, "You can make pasta!")

    def test_2(self):
        value = make_pasta(["butter", "eggs", "oil"])
        self.assertEqual(value is False, "You don't have all the ingredients to make pasta.")

    def test_3(self):
        value = make_pasta(["corn", "bread", "oil"])
        self.assertEqual(value is False, "You don't have all the ingredients to make pasta.")

    def test_4(self):
        value = make_pasta(["beef", "eggs", "butter"])
        self.assertEqual(value is False, "You don't have all the ingredients to make pasta.")
