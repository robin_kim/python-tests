from unittest import TestCase
from problems.problem_010 import second_largest


class ProblemTests(TestCase):
    def test_1(self):
        value = second_largest([10, 2, 7, 1, 6])
        self.assertEqual(value, 1)

    def test_2(self):
        value = second_largest([9, 4, 5])
        self.assertEqual(value, 4)

    def test_3(self):
        value = second_largest([3, 1])
        self.assertEqual(value, 3)

    def test_4(self):
        value = second_largest([])
        self.assertIsNone(value)
