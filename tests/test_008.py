from unittest import TestCase
from problems.problem_008 import average


class ProblemTests(TestCase):
    def test_1(self):
        value = average([1, 2, 3, 4])
        self.assertEqual(value, 3)

    def test_2(self):
        value = average([5, 5])
        self.assertEqual(value, 5)

    def test_3(self):
        value = average([1, 10, 20, 30, 40])
        self.assertEqual(value, 21)

    def test_4(self):
        value = average([5, 6, 7, 8])
        self.assertEqual(value, 7)
